

$(document).bind("ready", function() {
        //     navigator.splashscreen.hide(); //Hides the splash screen for your app.
        // StatusBar.overlaysWebView(false); //Turns off web view overlay.
            $("#page10").delegate("#RIMselect", "change", function() {
                var txt = $.trim($('#RIMselect').find(":selected").text());
                // alert("changed to" + txt);
                console.log(txt);
                if (txt == "All patient groups") {
                    $('#allPatientGroups').show();
                    $('#NeuromuscularBlockers').hide();
                    $('#Sedation').hide();
                    $('#Obstetrics').hide();
                    $('#Cardiothoracics').hide();
                    $('#Paediatrics').hide();
                }
                if (txt == "Neuromuscular blockers") {
                    $('#allPatientGroups').hide();
                    $('#NeuromuscularBlockers').show();
                    $('#Sedation').hide();
                    $('#Obstetrics').hide();
                    $('#Cardiothoracics').hide();
                    $('#Paediatrics').hide();
                }
                if (txt == "Obstetrics") {
                    $('#allPatientGroups').hide();
                    $('#NeuromuscularBlockers').hide();
                    $('#Sedation').hide();
                    $('#Obstetrics').show();
                    $('#Cardiothoracics').hide();
                    $('#Paediatrics').hide();
                }
                if (txt == "Paediatrics") {
                    $('#allPatientGroups').hide();
                    $('#NeuromuscularBlockers').hide();
                    $('#Sedation').hide();
                    $('#Obstetrics').hide();
                    $('#Cardiothoracics').hide();
                    $('#Paediatrics').show();
                }
                if (txt == "Sedation") {
                    $('#allPatientGroups').hide();
                    $('#NeuromuscularBlockers').hide();
                    $('#Sedation').show();
                    $('#Obstetrics').hide();
                    $('#Cardiothoracics').hide();
                    $('#Paediatrics').hide();
                }
                if (txt == "Cardiothoracics") {
                    $('#allPatientGroups').hide();
                    $('#NeuromuscularBlockers').hide();
                    $('#Sedation').hide();
                    $('#Obstetrics').hide();
                    $('#Cardiothoracics').show();
                    $('#Paediatrics').hide();
                }
            });
            $( "#page10" ).on( "pagecreate", function( event, ui ) {
                //alert("pagecreated")
                    $('#allPatientGroups').show();
                    $('#NeuromuscularBlockers').hide();
                    $('#Sedation').hide();
                    $('#Obstetrics').hide();
                    $('#Cardiothoracics').hide();
                    $('#Paediatrics').hide();
            });
        });